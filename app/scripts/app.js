'use strict';

angular.module('hbApp', ['ui.bootstrap', 'hbDirective', 'hbService', 'hbFilter'])
  .config(function($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
