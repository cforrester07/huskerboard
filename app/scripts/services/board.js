'use strict';


angular.module('hbService', ['ngResource']).factory('boardService',
   function($resource) {

        return $resource('scripts/:boardId.json', {}, {
            query: {
                method: 'GET',
                params: {
                    boardId: 'board'
                }
            }
        });

    }

);



// create: function(boardName) {
//                 var name = boardName;
//                 var data2 = [];
//                 for (var i = 0; i < 100; i++) {
//                     data2.push({
//                         name: '',
//                         tokens: 25,
//                         editmode: 'false',
//                         actualValue: {
//                             home: 0,
//                             away: 0
//                         }
//                     });
//                 }

//                 return {
//                     boardname: name,
//                     data: data2
//                 };

//             }
