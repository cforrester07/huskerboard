'use strict';

angular.module('hbApp')
    .controller('MainCtrl', function($scope, boardService) {

     
       boardService.get({boardId: 'board'}, function(board){
            $scope.board = board;
        });
        //console.log(boardService.load('board'));
        console.log($scope.board);
        $scope.currentEdit = 0;
        $scope.clickFunction = function(index) {
            var i = parseInt(index);
            if (i !== -1){
	            $scope.board.data[$scope.currentEdit].editmode = false;
	            $scope.currentEdit = i;
	            $scope.board.data[i].editmode = true;
            } else {
            	$scope.board.data[$scope.currentEdit].editmode = false;
            }
        console.log(JSON.stringify($scope.board));
        }

        $scope.doneFunction = function(index) {
            var i = parseInt(index);
            $scope.board.data[i].editmode = false;
        }



    });
